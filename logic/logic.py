import paho.mqtt.client as mqtt
import datetime
import enum
import json
import traceback
import re

MIN_POWER = 50

class State(enum.Enum):
    OFF = 1
    BREW = 2
    KEEPWARM = 3


class CoffeeMaker:
    uuid  = None

    state = None
    state_changed = None
    updated       = None

    last_temp  = None
    last_power = None
    max_power  = None

    avg_brew_time = None
    avg_brew_samples = None

    avg_keep_time = None
    avg_keep_samples = None

    def __init__(self, uuid: str):
        self.uuid = uuid

    def update(self, data: dict) -> None:
        """
        :param data: Data from MQTT

        Parses an payload from MQTT sent by the sensors
        and updates internal state
        """
        ts = datetime.datetime.now()
        old_state = self.state

        # Record the time of last update
        self.updated = ts

        # Try getting current power from the data payload
        p = data.get('power', None)
        if p:
            self.last_power = p

            # Keep track of the max recorded power value
            if not self.max_power or p > self.max_power:
                self.max_power = p

            # Estimate machine state based on the current power
            if p < MIN_POWER:
                self.state = State.OFF
            else:
                if p > 0.8 * self.max_power:
                    self.state = State.BREW
                else:
                    self.state = State.KEEPWARM

        # Try getting current temperature from the payload
        t = data.get('temperature', None)
        if p:
            self.last_temp = t

        # Run extra logic on state changes
        if self.state is not old_state:

            # BREW state done, record the time spent
            if old_state == State.BREW and self.state == State.KEEPWARM:
                brew_time = ts - self.state_changed
                if self.avg_brew_time:
                    self.avg_brew_time = ((self.avg_brew_time * self.avg_brew_samples) + brew_time) / (self.avg_brew_samples + 1)
                    self.avg_brew_samples   = self.avg_brew_samples + 1
                else:
                    self.avg_brew_time = brew_time
                    self.avg_brew_samples = 1

            # KEEPWARM state done, record the time spent
            if old_state == State.KEEPWARM and self.state == State.OFF:
                keep_time = ts - self.state_changed
                if self.avg_keep_time:
                    self.avg_keep_time = ((self.avg_keep_time * self.avg_keep_samples) + keep_time) / (self.avg_keep_samples + 1)
                    self.avg_keep_samples   = self.avg_keep_samples + 1
                else:
                    self.avg_keep_time = keep_time
                    self.avg_keep_samples = 1

            # Record the time of last state change
            self.state_changed = ts
            print("State changed to", self.state)

    def send(self, client) -> None:
        """
        :param client: MQTT client to use for sending the update

        Send information about the machine state and estimates
        to MQTT.
        """
        elapsed = datetime.datetime.now() - self.state_changed if self.state_changed else None

        if self.state == State.BREW and self.avg_brew_time:
            estimate = self.avg_brew_time - elapsed
            if estimate.days < 0:
                estimate = datetime.timedelta(seconds=0)
        elif self.state == State.KEEPWARM and self.avg_keep_time:
            estimate = self.avg_keep_time - elapsed
            if estimate.days < 0:
                estimate = datetime.timedelta(seconds=0)
        else:
            estimate = None

        client.publish(f'/coffeemaker/{self.uuid}/info', payload=json.dumps({
            "state": self.state.name,
            "temperature": self.last_temp,
            "state_elapsed": elapsed,
            "state_estimate": estimate,
        }, default=str))


coffeemakers = {}
def handle_data(client, uuid: str, data: dict) -> None:
    if uuid not in coffeemakers.keys():
        coffeemakers[uuid] = CoffeeMaker(uuid)
    coffeemakers[uuid].update(data)
    coffeemakers[uuid].send(client)


# The callback for when the client receives a CONNACK response from the server.
def on_connect(client, userdata, flags, rc) -> None:
    print("Connected with result code "+str(rc))

    # Subscribing in on_connect() means that if we lose the connection and
    # reconnect then subscriptions will be renewed.
    client.subscribe("/coffeemaker/+")

# The callback for when a PUBLISH message is received from the server.
def on_message(client, userdata, msg) -> None:
    try:
        uuid = re.search('^/coffeemaker/(.*)', msg.topic).group(1)
        data = json.loads(msg.payload)
        handle_data(client, uuid, data)
    except:
        traceback.print_exc()

client = mqtt.Client()
client.on_connect = on_connect
client.on_message = on_message

client.connect("mqtt.iot-coffee.exception.fi", 1883, 60)

# Blocking call that processes network traffic, dispatches callbacks and
# handles reconnecting.
# Other loop*() functions are available that give a threaded interface and a
# manual interface.
client.loop_forever()
