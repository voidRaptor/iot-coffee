#include <math.h>


/*
 *
 *
 * +5v o----+
 *          |
 *          R1
 *          |
 *          +--- A0 (analog in)
 *          |
 *          RT
 *          |
 *  0v o----+
 *
 *
 * R1 = 8200 ohmia
 * RT = ~10K termistori (NTC)
 *
*/

void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600);
  pinMode(A0, INPUT);
}

void loop() {
  static int suurin = 0;
  static int pienin = 1023;
  // put your main code here, to run repeatedly:
  int sample = analogRead(A0);
  if (sample > suurin) {suurin = sample;}
  if (sample < pienin) {pienin = sample;}

  int laskettu_lampotila = fit((double)sample);
  
  //Serial.print(sample);
  //Serial.print(", ");
  //Serial.print(suurin);
  //Serial.print(", ");
  //Serial.print(pienin);

  Serial.print("T");
  Serial.print(laskettu_lampotila);
  Serial.print("\n");
  delay(1000);

}

double fit(double sample)
{
  return -34.014 * log(sample) + 243.006;
}

/*
Taulukoituja arvoja: 

Läppärin tuuletin: 
333 = 40C
323 = 42C
312 = 45C

Huoneenlämpö alakerta: 
521 = 33C
526 = 32C
546 = 31C
550 = 30C
560 = 28C


Huoneenlämpö yläkerta:
575 = 25

Vedenkeittimen höyry:
73

Ruumiin lämpö: 
472 = 36C


 */
