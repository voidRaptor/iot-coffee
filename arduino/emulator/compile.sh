# !/bin/bash

rm prog.bin prog.hex &> /dev/null

if avr-gcc -Wall -Wextra -Wl,-u,vfprintf -lprintf_flt -Os -mmcu=atmega328p -DF_CPU=16000000 *.c -o prog.bin; then
    echo "***Success***"
else
    echo "***Failure***"
    exit 1
fi

avr-objcopy -j .text -j .data -O ihex prog.bin prog.hex

exit 0
