#include <stdlib.h>
#include <avr/cpufunc.h>
#include <avr/interrupt.h>
#include <avr/io.h>

#include <stdio.h>
#include <limits.h>

#define FCPU 16000000  // 16MHz
#include <util/delay.h>


#define BAUD 9600
#define BAUDRATE (FCPU/16/BAUD - 1)

#define ADC_PIN 0 // PC0

#define DELAY 1.0f
#define TIMER1_CMP ( (uint16_t)(FCPU/256 * DELAY - 1) )
#define MIN_TEMP_DIFF 0.5f

volatile uint8_t timeout = 1;
volatile uint8_t dataReady = 0;
volatile uint8_t bufferData = 0;


void init_uart();
void init_timer();

/**
* @brief generate data to emulate coffeemaker
*
* @param str, string to fill with data
*/
void generateData(char* str, int t);
int randMinMax(int32_t min, int32_t max);
void restart_timer();
 
void uart_transmit(unsigned char data); 
unsigned char uart_receive(void);
void uart_transmit_str(char* str, uint8_t addNewline);




ISR(TIMER1_COMPA_vect) {
    timeout = 1;

    // stop and clear timer
    TCCR1B &= ~(1 << CS12) & ~(1 << CS11) & ~(1 << CS10);
    TCNT1 = 0;
}


ISR(USART_RX_vect) {
    cli();

    bufferData = UDR0;
    dataReady = 1;
    
    // if (ECHO) usart_transmit(bufferData);

    sei();
}


int main(void) {
    init_uart();
//    init_timer();
    
    sei();
    char str[20];
    int t = 0;
    
    // update loop
    while (1) {
        generateData(str, t);
        uart_transmit_str(str, 1);

        _delay_ms(1000);
        ++t;
    } 
    
    return 0;    
}


void init_uart() {
    
    // set baud rate
    UBRR0H = (BAUDRATE >> 8);                              
    UBRR0L = BAUDRATE;
    
    // enable tx and rx
    UCSR0B |= (1 << TXEN0) | (1 << RXEN0);
    
    // enable receive complete -interrupt
    UCSR0B |= (1 << RXCIE0);
    
    // stop bits: 1 (default)
    // data size: 8bit
    UCSR0C |= (1 << UCSZ00) | (1 << UCSZ01);
    
}


void init_timer() {

    // enable CTC mode, 16bit timer
    TCCR1A |= (1 << WGM12);

    // set max timer value, high byte first
    OCR1AH = (TIMER1_CMP >> 8);
    OCR1AL = TIMER1_CMP & 0xFF;

    // enable timer interrupt on compare match
    TIMSK1 |= (1 << OCIE1A);
    
    // set prescaler 256 and start
    TCCR1B |= (1 << CS12);
    
}


void uart_transmit(unsigned char data) {

    // wait until buffer is empty
    while ( !(UCSR0A & (1 << UDRE0)) );

    // send data
    UDR0 = data;
}


unsigned char uart_receive(void) {
    
    // wait for data
    while ( !(UCSR0A & (1 << RXC0)) );
    
    return UDR0;    

}


void uart_transmit_str(char* str, uint8_t addNewline) {

    while (*str) {
        uart_transmit(*str);
        ++str;
    }

    if (addNewline) {
        uart_transmit('\n');
    }

}


void restart_timer() {
    OCR1AH = (TIMER1_CMP >> 8);
    OCR1AL = TIMER1_CMP & 0xFF;
    TCCR1B |= (1 << CS12);
    timeout = 0;
}


void generateData(char* str, int t) {
    // [-5.0, 5.0]
    float r1 = (float)( randMinMax(-5000000, 5000000) ) / 1000000.0f;
    float power = 0.0f;

    if (0 <= t && t < 10*60) {
        power = 1000 + r1;

    } else if (10*60 <= t && t <= 20*60) {
        power = 250.0f + r1;

    } else {
        power = r1 * 0.001f;
        if (power < 0.0f) power = 0.0f;
    }


    sprintf(str, "%c%f", 'P', power);
}


int randMinMax(int32_t min, int32_t max) {
    return  min + rand() % (max-min+1);
}
