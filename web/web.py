import json
import re
from flask import Flask, render_template_string
from flask_mqtt import Mqtt


app = Flask(__name__)
app.config['MQTT_BROKER_URL'] = 'mqtt.iot-coffee.exception.fi'  # use the free broker from HIVEMQ
app.config['MQTT_BROKER_PORT'] = 1883  # default port for non-tls connection
app.config['MQTT_USERNAME'] = None  # set the username here if you need authentication for the broker
app.config['MQTT_PASSWORD'] = None  # set the password here if the broker demands authentication
app.config['MQTT_KEEPALIVE'] = 5  # set the time interval for sending a ping to the broker to 5 seconds
app.config['MQTT_TLS_ENABLED'] = False  # set TLS to disabled for testing purposes

mqtt = Mqtt(app)

states = {}

@mqtt.on_connect()
def handle_connect(client, userdata, flags, rc):
    print("MQTT connected")
    mqtt.subscribe('/coffeemaker/+/info')

@mqtt.on_message()
def handle_mqtt_message(client, userdata, message):
    uuid = re.search('^/coffeemaker/(.*)/info', message.topic).group(1)
    data = json.loads(message.payload)
    states[uuid] = data

@app.route('/')
def view():
    return render_template_string("""
    <h2>Coffee machines:</h2>
    {% for id in states %}
    <h3>{{ id }}</h3>
    State: {{ states[id]['state'] }}<br>
    Time in state: {{ states[id]['state_elapsed'] }}<br>
    Estimate: {{ states[id]['state_estimate'] }}<br>
    <br>
    Temperature: {{ states[id]['temperature'] }}<br>
    {% endfor %}
    """, states=states)

if __name__ == '__main__':
    app.run(host='0.0.0.0')
