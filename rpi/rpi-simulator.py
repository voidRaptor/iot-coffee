import time
import random

from lib import MQTTClient, make_payload


def generate(t: int):
    r1 = random.random() * 10 - 5
    r2 = random.random() * 4 - 2

    if 0 <= t < 10*60:
        power = 1000 + r1
    elif 10*60 <= t <= 20*60:
        power = 250 + r1
    else:
        power = max(0, r1 * 0.001)

    if 0 <= t < 1*60:
        temperature = 20 + t + r2
    elif 1*60 <= t < 20*60:
        temperature = 80 + r2
    else:
        temperature = max(20, 80 - (t - 2*60) * (60 / 30*60)) + r2

    return power, temperature


def main():
    client = MQTTClient(suffix='-sim')

    t = 0
    while True:
        power, temperature = generate(t)
        client.publish(make_payload(power, temperature))
        t += 1
        time.sleep(1)


if __name__ == '__main__':
    main()
