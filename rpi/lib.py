import datetime
import json
import paho.mqtt.publish as mqtt_publish

MQTT_HOSTNAME = "mqtt.iot-coffee.exception.fi"
MQTT_PORT = 1883
MQTT_TOPIC_BASE = "/coffeemaker/"


def get_machine_id() -> str:
    with open('/etc/machine-id', 'r') as f:
        return f.readline().strip()


def make_payload(power: float, temperature: float):
    """
    :param power: Power
    :param temperature: Temperature
    """
    timestamp = datetime.datetime.now().timestamp()
    return json.dumps({
        "timestamp": timestamp,
        "power": power,
        "temperature": temperature
    })


class MQTTClient:
    def __init__(self, suffix=None):
        self.hostname = MQTT_HOSTNAME
        self.port = MQTT_PORT
        self.topic = MQTT_TOPIC_BASE + get_machine_id() + (suffix if suffix else '')

    def publish(self, payload: str) -> None:
        mqtt_publish.single(self.topic, payload, hostname=self.hostname, port=self.port)
