import serial
import sys
import time
import os

from lib import MQTTClient, make_payload

PORT = "/dev/ttyUSB0"


def testParse():
    """test function "parseData"

    NOTE: will quit the program

    """
    data = "T200.20 P3000 T150 T12 Q34343 P2555\n"
    print("data: '", data, "'", sep="")
    print(parseData(data))

    data = "\n"
    print("data: '", data, "'", sep="")
    print(parseData(data))

    data = ""
    print("data: '", data, "'", sep="")
    print(parseData(data))

    data = "T-200\n"
    print("data: '", data, "'", sep="")
    print(parseData(data))

    data = "T200"
    print("data: '", data, "'", sep="")
    print(parseData(data))

    data = "T P"
    print("data: '", data, "'", sep="")
    print(parseData(data))

    quit()



def convert(item):
    """convert from raw item to actual value

    params:
        item (str): <type_as_char><value>

    return:
        converted value (float)
    """
    converted = None

    try:
        converted = float( item[1:] )

    except ValueError:
        print("incorrect measurement")

    return converted


def parseData(data):
    """parse data to json format

    params:
        data (str): data to parse

    return:
        data as json object

    from:
        <type_as_char><value> ...\n
        (only one measurement of each type on the same line)

    to:
        {
            "timestamp": 424524567,
            "temperature": 200.2324,
            "power": 65657.0
        }


    """
    parsed = data.split(" ")

    timestamp = int( time.mktime( time.localtime() ) )
    temp = None
    power = None

    # build elements for json
    for item in parsed:

        if ( len(item) == 0 ):
            print("empty measurement")
            continue

        if item[0] == "T":
            if ( temp != None ):
                print("multiple measurements of the same type!")
                continue

            temp = convert(item)

        elif item[0] == "P":
            if ( power != None ):
                print("multiple measurements of the same type!")
                continue

            power = convert(item)
        else:
            print("unknown measurement '", item, "'", sep="")

    return make_payload(power, temp)
    

def main():

    try:
        # baud 9600, 8 databits, no parity, 1 stopbit 
        arduino = serial.Serial(PORT)

    except serial.serialutil.SerialException:
        print("Could not open serial '", PORT, "'", sep="")
        quit()

    client = MQTTClient()

    # for debugging
    # testParse()
    
    data = ""
    while True:
        # TODO: specify a timeout?  when opening the serial port otherwise it 
        # could block forever if no newline character is received.
        data = arduino.readline().decode("utf-8")

        # remove trailing newline TODO: not needed?
        #data = data[0:len(data)-1]
        #print(data);

        payload = parseData(data)
        client.publish(payload)
        
        time.sleep(5)

        
if __name__ == "__main__":
    main()    
